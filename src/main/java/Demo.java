import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

public class Demo {
    public static void main(String[] args) {
        Person michael = new Person("Michael", "Jackson", LocalDate.now().minusYears(20), 70, 1.83f, Gender.MALE);
        Person gabriel = new Person("Gabriel", "Jackson", LocalDate.now().minusYears(23), 70, 1.83f, Gender.OTHER);
        Person john = new Person("John", "Jackson", LocalDate.now().minusYears(13), 120, 1.83f, Gender.FEMALE);

        Person[] people = {michael, gabriel, john};
        Stream<Person> peopleStream = Arrays.stream(people);
        peopleStream
                .filter(p -> p.getAge() >= Person.ADULT_AGE)
                .forEach(System.out::println);
        //Person[] adultsArray = adultsStream.toArray(Person[] :: new);
        System.out.println("Nauja eilute");
        System.out.println("Prideta eilute per IntelliJ");
     }
}
